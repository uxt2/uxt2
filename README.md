# UXT2

Super-light XFCE built from 18.04.3 netboot with only the required packages to run XFCE efficiently, nothing else. This leaves room for your demanding apps and work process. Designed to stay out of your way and let you get things done.

Features a host of under-the-hood improvements making your system snappy even on low-end hardware. Software includes a basic but useful selection of packages. A concept 3 years in the making which has been tested && refined && used as a daily driver.